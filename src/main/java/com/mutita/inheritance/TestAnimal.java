/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.inheritance;

/**
 *
 * @author Admin
 */
public class TestAnimal {

    public static void main(String[] arg) {
        Animal animal = new Animal("Ani", "white",0);
        animal.speak();
        animal.walk();

        Dog dang = new Dog("Dang", "Black&white");
        Dog To = new Dog("To", "Orange");
        Dog Mome = new Dog("Mome", "Black&white");
        Dog Bat = new Dog("Bat", "Black&white");
        dang.speak();
        dang.walk();
        
        Cat Zero = new Cat("Zero","Orange");
        Zero.speak();
        Zero.walk();
        
        
        Duck zom = new Duck("zom","Orange");
        Duck GabGab = new Duck("GabGab","Black");
        zom.speak();
        zom.walk();
        zom.fly();
        
        
        System.out.println("Zom is Animal: "+ (zom instanceof Animal));
        System.out.println("Zom is Dog: "+ (zom instanceof Duck));
        System.out.println("Zom is Cat: "+ (zom instanceof Object));
        System.out.println("Animal is Dog: "+ (animal instanceof Dog));
        System.out.println("Animal is Animal: "+ (animal instanceof Animal));
        
        Animal ani1 =null;
        Animal ani2 =null;
        ani1=zom;
        ani2=Zero;
        
        System.out.println("Ani1: zom is Duck "+(ani1 instanceof Duck));
        
        Animal[] animals = { dang,Zero,zom,To,Bat,GabGab,Mome};
        for(int i=0; i< animals.length;i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
               Duck duck = (Duck)animals[i];
               duck.fly();
            }
        }
    }
}
